package main

import (
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/chu4ng/word-stat/api"
	"gitlab.com/chu4ng/word-stat/pkg/storer/pg"
	"gitlab.com/chu4ng/word-stat/service"
	"gitlab.com/chu4ng/word-stat/utils"
)

func main() {
	dbCreds := utils.GetDBCredsFromEnv()
	updateInterval := time.Duration(utils.GetIntFromEnvWithFallback("UPDATE_INTERVAL", utils.UpdateIntervalFallback)) * time.Minute

	db, err := pg.NewStorer(dbCreds)
	if err != nil {
		log.Fatal(err)
	}
	service := service.NewService("https://jsonplaceholder.typicode.com/comments", db)
	handler := api.NewHandler(service)
	// Grab comments at the start of programm
	err = service.ProcessComments()
	if err != nil {
		log.Print(err)
	}
	service.IntervalUpdate(updateInterval)
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.GET("/post/:postId/comments/statistics", handler.ByPost)

	err = r.Run(os.Getenv("APP_ADDR"))
	if err != nil {
		log.Fatalf("Unable to run the server: %s\n", err)
	}
}
