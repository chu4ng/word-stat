package pg

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/chu4ng/word-stat/pkg/storer"
	"gitlab.com/chu4ng/word-stat/types"
)

type pgStorer struct {
	client *pg.DB
}

// GetByPost implements storer.Storer.
func (s *pgStorer) GetByPost(pid int) (types.PostStat, error) {
	p := types.PostStat{PostID: pid}
	err := s.client.Model(&p).WherePK().Select()
	if err != nil {
		return types.PostStat{}, fmt.Errorf("db: no post with id %d", pid)
	}
	return p, nil
}

func (s *pgStorer) SavePosts(cs ...types.PostStat) error {
	_, err := s.client.Model(&cs).OnConflict("(post_id) DO UPDATE").Insert()
	return err
}

type Creds struct {
	Username string
	DBName   string
	Password string
	Timeout  int
	Address  string
}

func newPgClient(creds *Creds) (*pg.DB, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(creds.Timeout)*time.Second)
	defer cancel()
	db := pg.Connect(&pg.Options{
		User:     creds.Username,
		Database: creds.DBName,
		Password: creds.Password,
		Addr:     creds.Address,
	})
	if err := db.Ping(ctx); err != nil {
		return nil, err
	}
	err := createSchema(db)
	return db, err
}

func createSchema(db *pg.DB) error {
	err := db.Model(&types.PostStat{}).CreateTable(&orm.CreateTableOptions{
		Temp:        false,
		IfNotExists: true,
	})
	return err
}

func NewStorer(cred *Creds) (storer.Storer, error) {
	repo := &pgStorer{}
	client, err := newPgClient(cred)
	repo.client = client
	return repo, err
}
