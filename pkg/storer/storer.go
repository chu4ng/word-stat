package storer

import "gitlab.com/chu4ng/word-stat/types"

type Storer interface {
	SavePosts(...types.PostStat) error
	GetByPost(int) (types.PostStat, error)
}
