package api

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/chu4ng/word-stat/service"
)

type StatHandler interface {
	ByPost(c *gin.Context)
}

type statHandler struct {
	service service.StatService
}

func NewHandler(s service.StatService) StatHandler {
	return &statHandler{
		service: s,
	}
}

func (sh statHandler) ByPost(c *gin.Context) {
	strID := c.Param("postId")
	postID, err := strconv.Atoi(strID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"error": fmt.Sprintf("'%s' is not a number", strID),
		})
		return
	}
	ps, err := sh.service.GetByPost(postID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": fmt.Sprintf("no post with an ID %d", postID),
		})
		return
	}
	c.JSON(http.StatusOK, ps)
}
