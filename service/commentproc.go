package service

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.com/chu4ng/word-stat/types"
)

type WordsPoolPerPost map[int][]string

func (s *statService) ProcessComments() error {
	log.Printf("Processing comments")
	comments, err := s.GrabComments()
	if err != nil {
		return err
	}
	postsPool := WordsPoolPerPost{}
	for _, comm := range comments {
		pID := comm.PostID
		comm.Body = strings.ReplaceAll(comm.Body, "\n", " ")
		words := strings.Split(comm.Body, " ")
		postsPool[pID] = append(postsPool[pID], words...)
	}

	return s.SaveStats(postsPool)
}

func (s *statService) GrabComments() ([]types.Comment, error) {
	r, err := http.Get(s.sourceLink)
	if err != nil {
		return nil, fmt.Errorf("service: unable to load the page %s: %w", s.sourceLink, err)
	}
	defer r.Body.Close()

	comments := []types.Comment{}

	if err = json.NewDecoder(r.Body).Decode(&comments); err != nil {
		return nil, err
	}
	return comments, nil
}
