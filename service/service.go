package service

import (
	"fmt"
	"log"
	"sort"
	"time"

	"gitlab.com/chu4ng/word-stat/pkg/storer"
	"gitlab.com/chu4ng/word-stat/types"
)

type StatService interface {
	GetByPost(int) ([]types.CommentStat, error)
	ProcessComments() error
	IntervalUpdate(time.Duration) chan bool
}

type statService struct {
	sourceLink string
	storer     storer.Storer
}

func (s *statService) SaveStats(pp WordsPoolPerPost) error {
	var stats []types.PostStat
	for pid, words := range pp {
		ps := types.PostStat{
			PostID: pid,
			Words:  words,
		}
		stats = append(stats, ps)
	}
	if err := s.storer.SavePosts(stats...); err != nil {
		return fmt.Errorf("service: unable to save stats: %w", err)
	}
	return nil
}

func (s *statService) GetByPost(pid int) ([]types.CommentStat, error) {
	post, err := s.storer.GetByPost(pid)
	if err != nil {
		return nil, err
	}
	return WordCount(&post), nil
}

func WordCount(post *types.PostStat) []types.CommentStat {
	wc := map[string]int{}
	for _, v := range post.Words {
		if _, ok := wc[v]; !ok {
			wc[v] = 1
		} else {
			wc[v]++
		}
	}
	var stats []types.CommentStat
	for word, count := range wc {
		s := types.CommentStat{
			PostID: post.PostID,
			Word:   word,
			Count:  count,
		}
		stats = append(stats, s)
	}
	sort.Slice(stats, func(i, j int) bool {
		return stats[i].Count > stats[j].Count
	})
	return stats
}

func (s *statService) IntervalUpdate(timeout time.Duration) chan bool {
	ticker := time.NewTicker(timeout)
	quit := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				if err := s.ProcessComments(); err != nil {
					log.Printf("during interval update: %s", err)
				}
				log.Printf("Interval update\n")
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
	return quit
}

func NewService(srcLink string, db storer.Storer) StatService {
	return &statService{
		storer:     db,
		sourceLink: srcLink,
	}
}
