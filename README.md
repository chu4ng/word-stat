`wordstat` grabs comments from https://jsonplaceholder.typicode.com and count words from them, sorting words in descend frequency order.

GET /post/{postId}/comments/statistics:
```json
[
    {
        "postId": 1,
        "word": "el",
        "count": 9
    },
    {
        "postId": 1,
        "word": "est",
        "count": 7
    }
]
```

# Quick start

Build and run using docker-compose:
```sh
docker-compose up
```
From source code:
```sh
git clone https://gitlab.com/chu4ng/word-stat
cd word-stat

go get ./...
go build -o wordstat
./wordstat
```
NOTE: service requires running Postgresql's instance.
For configuration see `.env`:
```env
DB_USER="postgres"
DB_PASS="postgres"
DB_NAME="wordstat"
DB_ADDR="127.0.0.1:5432"

APP_ADDR=":3000"
# Timeout in minutes for updating comments
UPDATE_INTERVAL=5
```

