build:
	go build -o wordstat ./main.go 
run: build
	./wordstat
lint:
	golangci-lint -c .golangci-lint.yml run ./...
