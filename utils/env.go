package utils

import (
	"os"
	"strconv"

	"gitlab.com/chu4ng/word-stat/pkg/storer/pg"
)

const DBTimeoutFallback = 3
const UpdateIntervalFallback = 5

func GetDBCredsFromEnv() *pg.Creds {
	dbCreds := &pg.Creds{
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASS"),
		DBName:   os.Getenv("DB_NAME"),
		Address:  os.Getenv("DB_ADDR"),
		Timeout:  GetIntFromEnvWithFallback("DB_TIMEOUT", DBTimeoutFallback),
	}
	return dbCreds
}

func GetIntFromEnvWithFallback(env string, fallback int) int {
	envValStr := os.Getenv(env)
	envNum, err := strconv.Atoi(envValStr)
	if err != nil {
		return fallback
	}
	return envNum
}
