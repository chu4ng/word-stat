package types

type CommentStat struct {
	PostID int
	Word   string
	Count  int
}

type Comment struct {
	PostID int    `json:"postId"`
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Body   string `json:"body"`
}

type PostStat struct {
	PostID int `pg:",pk"`
	Words  []string
}
